const debug = require('debug')('rtu-client-response-handler')
const ModbusRTUResponse = require('./rtu-response.js')
const ModbusClientResponseHandler = require('./client-response-handler.js')

/** Modbus/RTU Client Response Handler
 * @extends ModbusClientResponseHandler
 * @class
 */
class ModbusRTUClientResponseHandler extends ModbusClientResponseHandler {
  constructor(address) {
    super();
    this._address = address;
  }
  handleData (data) {
    debug('receiving new data')
    this._buffer = Buffer.concat([this._buffer, data])

    debug('buffer', this._buffer)

    while (this._buffer.length > 0) {
      let address = this._buffer.readUInt8(0)
      if (address != this._address) {
        debug('Throw unmatched address byte', address.toString('16'));
        this._buffer = this._buffer.slice(1);
      }
      else {
        break;
      }
    }
    if (this._buffer.length < 1) {
      return;
    }
    do {
      const response = ModbusRTUResponse.fromBuffer(this._buffer)

      if (!response) {
        debug('not enough data available to parse')
        return
      }

      debug('crc', response.crc)

      debug('reset buffer from', this._buffer.length, 'to', (this._buffer.length - response.byteCount))

      /* reduce buffer */
      this._buffer = this._buffer.slice(response.byteCount)

      this._messages.push(response)
    } while (1)
  }

  shift () {
    return this._messages.shift()
  }
}

module.exports = ModbusRTUClientResponseHandler
