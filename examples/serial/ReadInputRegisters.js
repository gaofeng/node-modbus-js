'use strict'

const modbus = require('../..')
const Serialport = require('serialport')
const socket = new Serialport('COM1', {
  baudRate: 115200,
  Parity: 'none',
  stopBits: 1,
  dataBits: 8
})

// set Slave PLC ID
const client = new modbus.client.RTU(socket, 1)

socket.on('open', function () {
  client.readInputRegisters(0, 12).then(function (resp) {
    console.log(resp)
    socket.close()
  }, function (err) {
    console.log(err)
    socket.close()
  })
})

socket.on('error', function (err) {
  console.log(err)
})
