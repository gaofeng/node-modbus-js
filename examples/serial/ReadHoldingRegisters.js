const modbus = require('../../')
const SerialPort = require('serialport')
const socket = new SerialPort('COM2', {
  baudRate: 115200,
  parity: 'none',
  stopbits: 1
})

const client = new modbus.client.RTU(socket, 1)

socket.on('close', function () {
  console.log('close')
})

socket.on('open', function () {
  client.readHoldingRegisters(1, 1)
    .then(function (resp) {
      console.log(resp)
      socket.close()
    }).catch(function () {
      console.error('catch',arguments)
      socket.close()
    })
})

socket.on('data', function () {
  console.log('on data:', arguments)
})

socket.on('error', console.error)
